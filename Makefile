PROG = z

BINDIR   = /usr/local/bin
#BINDIR  = $(HOME)/bin
MKBINDIR = true
BINMODE  = 755

MANDIR   = /usr/local/man/man1
#MANDIR  = $(HOME)/man/man1
MKMANDIR = true
MANEXT   = .1
MANMODE  = 644

all:

install:
	-$(MKBINDIR) && mkdir $(BINDIR) 2> /dev/null
	cp $(PROG) $(BINDIR)/.
	chmod $(BINMODE) $(BINDIR)/$(PROG)

install.zzcat:
	-$(MKBINDIR) && mkdir $(BINDIR) 2> /dev/null
	cp zzcat $(BINDIR)/.
	chmod $(BINMODE) $(BINDIR)/zzcat

install.man:
	-$(MKMANDIR) && mkdir $(MANDIR) 2> /dev/null
	cp $(PROG).man $(MANDIR)/$(PROG)$(MANEXT)
	chmod $(MANMODE) $(MANDIR)/$(PROG)$(MANEXT)

deinstall: deinstall.z deinstall.zzcat

deinstall.z:
	rm -f $(BINDIR)/$(PROG)
	-$(MKBINDIR) && rmdir $(BINDIR) 2> /dev/null

deinstall.zzcat:
	rm -f $(BINDIR)/zzcat
	-$(MKBINDIR) && rmdir $(BINDIR) 2> /dev/null

deinstall.man:
	rm -f $(MANDIR)/$(PROG)$(MANEXT)
	-$(MKMANDIR) && rmdir $(MANDIR) 2> /dev/null

clean:

# -----------------------------------------------------------------------
# for maintainer use only

import:
	cp $(HOME)/binp/$(PROG) $(HOME)/binp/zzcat .
	cp $(HOME)/manp/man1/$(PROG).1 $(PROG).man
	cp $(HOME)/manp/man1/$(PROG).html .

dist:
	VER=`sed -n 's/^version=\([^ 	]*\).*/\1/p' $(PROG) | tail -1`;\
	BASE=$(PROG)-$$VER;\
	rm -f ../$$BASE.tgz;\
	cp -r ../src ../$$BASE;\
	z -gz -s .tgz -m 644 ../$$BASE
