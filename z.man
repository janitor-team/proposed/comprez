.TH Z 1 2.7.3
.SH NAME
z \- safely (un)tar and (de)feather files and directories
.SH SYNOPSIS
.B z
[
.B \-t
|
.B \-T
] [
.B \-v
|
.B \-V
] [
.B \-l
|
.B \-L
] [
.B \-gz
|
.B \-z
|
.B \-I
|
.B \-lz
|
.B \-xz
|
.B \-Z
|
.B \-zip
|
.B \-jar
] [
.B \-#
] [
.B \-s
.I suffix
] [
.B \-m
.I mode
] [
.B \-p
|
.B \-P
] [
.B \-h
] [
.B \-\-
]
.I file ...
.SH DESCRIPTION
.I Z
is a simple, safe and convenient front-end for the
.IR compress (1),
.IR uncompress (1),
.IR gzip (1),
.IR bzip2 (1),
.IR lzip (1),
.IR xz (1),
.IR tar (1),
.IR zip (1)
and
.IR unzip (1)
utilities for compressing and uncompressing files and directories.
It processes each of its arguments according to the type of the
file or directory given:
.PP
If the argument is a plain file, then the file is compressed,
ie, feathered.
.PP
If the argument is a compressed file
with a name ending in \fB.Z\fP, \fB.gz\fP, \fB.z\fP, \fB.bz2\fP,
\fB.lz\fP, \fB.xz\fP, \fB.zip\fP or \fB.jar\fP,
then the file is uncompressed,
ie, defeathered.
.PP
If the argument is a directory,
then the directory is archived into one tar or zip file
which is then compressed,
ie, tarred and feathered.
.PP
If the argument is a compressed tar or zip archive
with a name ending in \fB.{tar.,tar,ta,t}{Z,gz,z,bz2,lz,xz}\fP or
\fB.{zip,jar}\fP, then the archive is uncompressed and untarred,
ie, untarred and defeathered.
.PP
The new compressed or uncompressed version will be in the same
directory as the original.
A compressed file is always uncompressed into a file with the
same name sans the feather suffix.
A compressed tar or zip archive is always unpacked into a subdirectory
with the same name as the archive sans the tar and feather suffix,
even if the archive did not itself contain such a subdirectory.
.SH OPTIONS
.PP
.TP 10
\-\-
Interprets all following arguments as files instead of options.
.TP 10
\-#
Where \fB#\fP is a digit from 1 through 9.
This option is passed on to
.IR gzip (1),
.IR bzip2 (1),
.IR lzip (1),
.IR xz (1)
and
.IR zip (1)
when feathering with them.
\fB\-\-fast\fP may be used instead of \fB\-1\fP and
\fB\-\-best\fP instead of \fB\-9\fP.
.TP 10
\-gz
Uses
.IR gzip (1)
and a \fB.gz\fP compression suffix when feathering.
Default.
.TP 10
\-h
Prints a helpful usage message.
.TP 10
\-I
Uses
.IR bzip2 (1)
and a \fB.bz2\fP compression suffix when feathering.
.TP 10
\-l
Lists the file or directory name created resulting from each argument.
.TP 10
\-L
Does not report created files or directories.
Default.
.TP 10
\-lz
Uses
.IR lzip (1)
and a \fB.lz\fP compression suffix when feathering.
.TP 10
\-xz
Uses
.IR xz (1)
and a \fB.xz\fP compression suffix when feathering.
.TP 10
\-m mode
Apply the given
.IR chmod (1)
mode argument to any created tar and feather files.
Before this argument is applied,
the files have the same read and write permissions
as the directories from which they were created.
For security, the default argument is \fBgo\-rwx\fP.
If the mode is \fB\-\fP, then no argument is applied.
See
.IR chmod (1)
for all other allowable formats of this argument.
.TP 10
\-p
Preserves modes when untarring by giving the \fBp\fP flag to
.IR tar (1).
Default.
.TP 10
\-P
Doesn't preserve modes when untarring by not giving the \fBp\fP flag to
.IR tar (1).
This option may be necessary on systems where ordinary users are
allowed to run
.IR chown (2).
.TP 10
\-s suffix
Creates tar and feather files using the given suffix style.
The default suffix style is \fB.tgz\fP.
Note that the suffix style does not dictate the program used
for feathering nor the compression suffix.
.TP 10
\-t
Only lists the table of contents of the given files.
Does not make any changes.
.TP 10
\-T
(Un)tars and (de)feathers the given files according to their type.
Default.
.TP 10
\-v
Verbose output.
For example, reports compression ratios when feathering.
.TP 10
\-V
Non-verbose output.
Does not report compression ratios.
Default.
\fB\-q\fP is a synonym.
.TP 10
\-z
Uses
.IR gzip (1)
and a \fB.z\fP compression suffix when feathering.
.TP 10
\-Z
Uses
.IR compress (1)
and a \fB.Z\fP compression suffix when feathering.
.TP 10
\-zip, \-jar
Uses
.IR zip (1)
and a \fB.zip\fP or \fB.jar\fP compression suffix
when tarring and/or feathering.
The zip format combines tarring and feathering.
\fBWARNING:\fP zip does not preserve complete Unix filesystem information
for the files it archives, such as links, some permissions, etc.
A
.IR tar (1)-based
format should be used if this is required.
.SH "ENVIRONMENT VARIABLES"
.PP
.TP 10
ZOPTS
This variable may be set to a string of the above options to
supersede the default settings.
They may still be overridden by options given on the command line.
.PP
.TP 10
GZIP
This environment variable for
.IR gzip (1)
is \fBnot\fP passed on by
.I z
so that
.IR gzip 's
behavior is standard and predictable.
.PP
.TP 10
ZIPOPT, UNZIP
These environment variables for
.IR zip (1)
and
.IR unzip (1)
are \fBnot\fP passed on by
.I z
so that their behavior is standard and predictable.
.SH BUGS
There should be an option to allow the use of another directory
for temporary files needed during the
(un)tarring and (de)feathering processes.
This would make
.I z
more useful when the quota or disk is nearly full.
.PP
There should be \fB\-k\fP and \fB\-K\fP options
for keeping the original input files or not.
.SH CAVEATS
.I Z
is written to work with
.I gzip
versions 1.2.3 and 1.2.4,
.I bzip2
versions 0.9.0 and 1.0.0,
Unix
.I zip
version 2.0.1 and Unix
.I unzip
version 5.12.
Other versions will probably work safely, too,
but should still be checked for compatibility.
.PP
Every effort has been made to assure that the use of this program
will not lead to the inappropriate deletion or corruption of any files.
However, there are never any guarantees,
so please use at your own risk.
.SH VERSION
2.7.3
.SH AUTHOR
Steve Kinzler, steve@kinzler.com, May 89/Jun 93/Aug 99/Dec 00
.SH URL
http://kinzler.com/me/z
.SH "SEE ALSO"
compress(1), uncompress(1), gzip(1), bzip2(1), lzip(1), xz(1), tar(1), zip(1),
unzip(1)
