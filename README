>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Z  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

                        http://kinzler.com/me/z

Z is a simple, safe and convenient front-end for the compress(1),
uncompress(1), gzip(1), bzip2(1), lzip(1), xz(1), tar(1), zip(1)
and unzip(1) utilities for compressing and uncompressing files and
directories. See the included man page for full details.

The basic idea behind z is that you can just run `z SOMETHING` where
SOMETHING is any file, directory, compressed file or compressed archive,
and z will do the right thing in a standard way.  Simple for novices and
convenient for experts.  Great for unpacking downloads or compressing
files to save space.

Running `z -h` will provide a usage summary.

----------------------------  INSTALLATION  ----------------------------

"z" is a shell script that you should be able to install as-is, ie
executable in a directory in your PATH.  A man page in both troff
("z.man") and HTML ("z.html") format is supplied.  You can install
these files manually as you wish or you can use the included Makefile
to install "z" and "z.man".  Just edit the top of the Makefile to your
desires and then run:

	make install
	make install.man

Alternately, on Debian/Ubuntu systems, you may be able to install "z",
but named "comprez", by installing the package "comprez":

	apt-get install comprez

---------------------------  GPL COPYRIGHT  ----------------------------

z
Copyright (C) 1989-2013 Stephen B Kinzler

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or any
later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with this program (see the file "COPYING"); if not, write to the Free
Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301 USA

---------------------  SOFTWARE BY STEVE KINZLER  ----------------------

Unix Home Dir	http://kinzler.com/me/home.html
		Many scripts and config files for Unix/X11 environments
align		http://kinzler.com/me/align
		A text column alignment filter
vshnu		http://kinzler.com/me/vshnu
		A visual shell and CLI shell supplement
webrowse	http://kinzler.com/me/webrowse
		Tools to interface web browsers to the Unix user CLI
xtitle		http://kinzler.com/me/xtitle
		Set window title and icon name for an X11 terminal window
z		http://kinzler.com/me/z
		A convenience frontend for tar, gzip, zip, etc

========================================================================

Steve Kinzler <steve@kinzler.com>			   December 2013
http://kinzler.com/me
